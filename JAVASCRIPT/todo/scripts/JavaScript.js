class Model {
  constructor() {
    this.todos = JSON.parse(localStorage.getItem("todos")) || [];
    this.filterValue = {
      all: "All",
      completed: "Completed",
    };
    this.filter = this.filterValue.all;
  }

  bindTodoListChanged(callback) {
    this.onTodoListRender = callback;
  }

  updateRender(todos = this.todos) {
    const filterArray = todos.filter((todo) => {
      if (this.filter === this.filterValue.all) return true;
      return this.filter === this.filterValue.completed
        ? todo.completed
        : !todo.completed;
    });
    this.onTodoListRender(filterArray);
    localStorage.setItem("todos", JSON.stringify(this.todos));
  }

  addTodo(text) {
    const todo = {
      id: Date.now(),
      title: text,
      completed: false,
    };
    this.todos.push(todo);
    this.updateRender(this.todos);
  }

  removeTodo(id) {
    this.todos = this.todos.filter((todo) => todo.id !== id);
    this.updateRender(this.todos);
  }

  toggleTodo(id) {
    this.todos = this.todos.map((todo) =>
      todo.id === id ? { ...todo, completed: !todo.completed } : todo
    );
    this.updateRender(this.todos);
  }

  editTodo(id, updatedText) {
    this.todos = this.todos.map((todo) =>
      todo.id === id ? { ...todo, title: updatedText } : todo
    );
    this.updateRender(this.todos);
  }

  filterTodo(selectedOptionValue) {
    this.filter = selectedOptionValue;
    this.updateRender();
  }
}

class View {
  constructor() {
    this.app = this.getElement("#root");
    this.form = this.createElement("form");
    this.header = this.createElement("header", "header");
    this.select = this.createElement("select", "select__filter");
    this.input = this.createElement("input", "form__input");
    this.todoList = this.createElement("ul", "lists");
    this.buttonAdd = this.createElement("button", "form__button");

    this.options = [
      { value: "All", text: "All" },
      { value: "Completed", text: "Completed" },
      { value: "NoCompleted", text: "No Completed" },
    ];

    for (let i = 0; i < this.options.length; i++) {
      const opt = this.createElement("option");
      opt.value = this.options[i].value;
      opt.innerHTML = this.options[i].text;
      this.select.appendChild(opt);
    }

    this.input.placeholder = "add todo";
    this.input.type = "text";
    this.buttonAdd.textContent = "Добавить";

    this.header.append(this.select);
    this.form.append(this.input, this.buttonAdd);
    this.app.append(this.header, this.form, this.todoList);

    this.editInput = "";
    this.initLocalInputValue();
  }

  resetInput() {
    this.input.value = "";
  }

  get _todoText() {
    return this.input.value;
  }

  renderTodo(todos) {
    while (this.todoList.firstChild) {
      this.todoList.removeChild(this.todoList.firstChild);
    }

    if (!todos.length) {
      const p = this.createElement("p", "title__hint");
      p.textContent = "No tasks!";
      this.todoList.append(p);
    } else {
      todos.map((item) => {
        const li = this.createElement("li", "list");
        const span = this.createElement("span");
        const checkbox = this.createElement("input");
        const buttonDelete = this.createElement("button", "list__button");

        li.id = item.id;
        buttonDelete.textContent = "Удалить";
        checkbox.type = "checkbox";
        checkbox.classList.add("checkbox__edit");
        checkbox.checked = item.completed;
        span.contentEditable = true;
        span.classList.add("editable");

        if (item.completed) {
          const stricke = this.createElement("s");
          stricke.textContent = item.title;
          span.append(stricke);
        } else {
          span.textContent = item.title;
        }

        li.append(checkbox, span, buttonDelete);
        this.todoList.append(li);
      });
    }
  }

  initLocalInputValue() {
    this.todoList.addEventListener("input", (event) => {
      if (event.target.className === "editable") {
        this.editInput = event.target.innerText;
      }
    });
  }

  bindAddTodo(handler) {
    this.form.addEventListener("submit", (event) => {
      event.preventDefault();
      if (this._todoText) {
        handler(this._todoText);
        this.resetInput();
      }
    });
  }

  bindDeleteTodo(handler) {
    this.todoList.addEventListener("click", (event) => {
      if (event.target.className === "list__button") {
        const id = parseInt(event.target.parentElement.id);
        handler(id);
      }
    });
  }

  bindToggleTodo(handler) {
    this.todoList.addEventListener("change", (event) => {
      if (event.target.type === "checkbox") {
        const id = parseInt(event.target.parentElement.id);
        handler(id);
      }
    });
  }

  bindEditTodo(handler) {
    this.todoList.addEventListener("focusout", (event) => {
      if (this.editInput) {
        const id = parseInt(event.target.parentElement.id);
        handler(id, this.editInput);
        this.editInput = "";
      }
    });
  }

  bindFilterTodo(handler) {
    this.select.addEventListener("click", () => {
      const selectedOptionValue =
        this.select.options[this.select.selectedIndex].value;
      handler(selectedOptionValue);
    });
  }

  createElement(tag, className) {
    const element = document.createElement(tag);

    if (className) element.classList.add(className);

    return element;
  }

  getElement(selector) {
    const element = document.querySelector(selector);

    return element;
  }
}

class Controller {
  constructor(model, view) {
    this.model = model;
    this.view = view;

    this.model.bindTodoListChanged(this.onTodoListRender);
    this.view.bindAddTodo(this.handleAddTodo);
    this.onTodoListRender(this.model.todos);
    this.view.bindDeleteTodo(this.handleDeleteTodo);
    this.view.bindToggleTodo(this.handleToggleTodo);
    this.view.bindEditTodo(this.handleEditTodo);
    this.view.bindFilterTodo(this.handleFilterTodo);
  }

  onTodoListRender = (todos) => {
    this.view.renderTodo(todos);
  };

  handleAddTodo = (todoText) => {
    this.model.addTodo(todoText);
  };

  handleDeleteTodo = (id) => {
    this.model.removeTodo(id);
  };

  handleToggleTodo = (id) => {
    this.model.toggleTodo(id);
  };

  handleEditTodo = (id, todoText) => {
    this.model.editTodo(id, todoText);
  };

  handleFilterTodo = (selectedOptionValue) => {
    this.model.filterTodo(selectedOptionValue);
  };
}

const app = new Controller(new Model(), new View());
