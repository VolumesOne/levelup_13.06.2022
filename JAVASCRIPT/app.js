// Третий задачник

// задача 1

const string = "Привет мир!";
const reversed = Array.prototype.map
  .call(string, (x) => x)
  .reverse()
  .join("");
console.log(reversed);

// задача 2

let helloName = (name) => `Hello ${name}!`;
console.log(helloName("Gerald"));

// задача 3

let year = null;

function calcAge(year) {
  return year * 365;
}

console.log(calcAge(65));

// задача 4

let arr = [1, 2, 3, 4, 5, 6];

function contains(arr, elem) {
  if (arr.find((i) => i === elem)) {
    return true;
  } else {
    return false;
  }
}
console.log(contains(arr, 6));

// задача 5

function numArgs() {
  console.log(arguments.length);
  for (let elem of arguments) {
    return elem;
  }
}
numArgs();

// задача 6

let num = null;

function NumsOddEven(num) {
  if (num % 2 == 0) {
    return "even";
  } else {
    return "odd";
  }
}

console.log(NumsOddEven(9));
