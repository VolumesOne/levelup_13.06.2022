import Header from "./components/Header/Header";
import Todos from "./components/Todos/Todos";
import Container from "@mui/material/Container";
import Category from "./components/Category/Category";
import Grid from "@mui/material/Grid";
import styles from './App.module.scss';
import Modals from './components/Modals/Modals';
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import ProgressBar from "./components/Progressbar/Progressbar";

function App() {
  const history = useNavigate();
  useEffect(()=>{
    history('/')
  },[]);
  return (
    <div>
      <Header />
      <Container maxWidth="xl">
       <ProgressBar/> 
        <Grid container spacing={2} className={styles.wrapper}>
          <Grid item xs={6} md={4}>
            <Todos />
          </Grid>
          <Grid item xs={6} md={8}>
            <Category />
          </Grid>
        </Grid>
      </Container>
      <Modals/>
    </div>
  );
}

export default App;
