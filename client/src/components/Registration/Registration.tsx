import React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { Link } from "react-router-dom";

function Registration() {
  return (
    <div className="container">
      <form className="area__form">
        <div className="area__contain">
          <p className="area__text">Регистрация</p>
          <div className="area__login">
            <TextField id="outlined-basic" label="Логин" variant="outlined" />
          </div>
          <div className="area__password">
            <TextField id="outlined-basic" label="Пароль" variant="outlined" />
          </div>
          <div className="area__repeat-password">
            <TextField
              id="outlined-basic"
              label="Повторить пароль"
              variant="outlined"
            />
          </div>
          <div className="button__enter">
            <Button className="btn" variant="outlined">
              Зарегистрироваться
            </Button>
          </div>
          <p className="text__link">
            <Link to="/Login">Если есть аккаунт, то авторизуйтесь!</Link>
          </p>
        </div>
      </form>
    </div>
  );
}

export default Registration;
