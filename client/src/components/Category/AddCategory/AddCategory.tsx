import React, { FC } from "react";
import { useForm, Controller } from "react-hook-form";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { useAppDispatch } from "../../../store/hook";
import { addCategory } from "../../../store/category/slice";
import styles from './addcategory.module.scss';

type CategoryForm = {
  category: string;
};

type AddCategoryProps = {
  todoId: number;
};

const AddCategory: FC<AddCategoryProps> = ({ todoId }) => {
  let schema = yup.object().shape({
    category: yup
      .string()
      .required("Поле не должно быть пустым")
      .test("todoTest", "Выберете сначала задачу", () => !!todoId),
  });

  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    defaultValues: {
      category: "",
    } as CategoryForm,
    resolver: yupResolver(schema),
  });

  const dispatch = useAppDispatch();

  const onSubmit = (data: CategoryForm) => {
    dispatch(addCategory({ ...data, todoId, id: Date.now() }));
    setValue("category", "");
  };

  return (
    <div>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Controller
          name="category"
          control={control}
          render={({ field }) => (
            <TextField
              {...field}
              helperText={errors?.category?.message}
              error={!!errors?.category}
              size="small"
              id="outlined-basic"
              label="Добавить подзадачу"
              variant="outlined"
            />
          )}
        />
        <Button variant="contained" type="submit">
          Добавить подзадачу
        </Button>
      </form>
    </div>
  );
};

export default AddCategory;
