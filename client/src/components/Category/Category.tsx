import React, { BaseSyntheticEvent, FC } from "react";
import { useMatch } from "react-router-dom";
import { useAppSelector, useAppDispatch } from "../../store/hook";
import AddCategory from "./AddCategory/AddCategory";
import Checkbox from "@mui/material/Checkbox";
import DeleteIcon from "@mui/icons-material/Delete";
import Typography from "@mui/material/Typography";
import EditIcon from "@mui/icons-material/Edit";
import styles from "./category.module.scss";
import { Box } from "@mui/system";
import { showModal, modalsEnums } from "../../store/modals/slice";
import { editCompletedCategory } from "../../store/category/slice";
import { ListItem, Lists } from "../Lists/Lists";

const Category: FC = () => {
  const { category } = useAppSelector((state) => state.category);

  const match = useMatch("todos/:id");
  const dispatch = useAppDispatch();
  const todoId = Number(match?.params?.id);

  const handleRemoveCategory = (categoryId: number, category: string) => {
    dispatch(
      showModal({ name: modalsEnums.modalDeleteCategory, categoryId, category })
    );
  };

  const handleCheckedCategory = (categoryId: number) => {
    dispatch(editCompletedCategory({ categoryId }));
  };

  return (
    <>
      <AddCategory todoId={todoId} />
      <Lists>
        {category.map((item) => {
          return todoId === item.todoId ? (
            <ListItem key={item.id}>
              {/* {!item.completed ? styles.list : styles.list_linethrough} */}
              <Box className={styles.category}>
                <Typography variant="subtitle1" gutterBottom>
                  {item.category}
                </Typography>
                <Box className={styles.groupItems}>
                  <EditIcon
                    onClick={() =>
                      dispatch(
                        showModal({
                          name: modalsEnums.modalEditCategory,
                          categoryId: item.id,
                        })
                      )
                    }
                  />
                  <Checkbox
                    checked={item.completed}
                    onChange={() => handleCheckedCategory(item.id)}
                  />

                  <DeleteIcon
                    onClick={() => handleRemoveCategory(item.id, item.category)}
                  />
                </Box>
              </Box>
            </ListItem>
          ) : null;
        })}
      </Lists>
    </>
  );
};

export default Category;
