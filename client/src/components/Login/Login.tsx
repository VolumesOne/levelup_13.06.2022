import React from "react";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { Link } from "react-router-dom";

function Login() {
  return (
    <div className="container">
      <form className="area__form">
        <div className="area__contain">
          <p className="area__text">Вход</p>
          <div className="area__login">
            <TextField id="outlined-basic" label="Логин" variant="outlined" />
          </div>
          <div className="area__password">
            <TextField id="outlined-basic" label="Пароль" variant="outlined" />
          </div>
          <div className="button__enter">
            <Button className="btn" variant="outlined">
              Войти
            </Button>
          </div>
          <p className="text__link">
            <Link to="/Registration">Нет аккаунта? Зарегистрируйтесь</Link>
          </p>
        </div>
      </form>
    </div>
  );
}

export default Login;
