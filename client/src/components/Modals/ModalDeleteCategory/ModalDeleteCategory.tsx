import React, { FC } from "react";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import styles from "./modalDeleteCategory.module.scss";
import { useAppDispatch } from "../../../store/hook";
import { hideModal, modalsEnums } from "../../../store/modals/slice";
import { removeCategory } from "../../../store/category/slice";

type modalDeleteCategoryProps = {
  name: string;
<<<<<<< Updated upstream
  category?: string;
  categoryId?: number;
=======
<<<<<<< Updated upstream
  category:string;
  categoryId:number;
=======
  description?: string;
  id?: number;
>>>>>>> Stashed changes
>>>>>>> Stashed changes
};

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

<<<<<<< Updated upstream
const ModalDeleteCategory: FC<modalDeleteCategoryProps> = ({
  name,
  category,
  categoryId,
}) => {
  const dispatch = useAppDispatch();
=======
<<<<<<< Updated upstream
const ModalDeleteCategory: FC<modalDeleteCategoryProps> = ({ name, category, categoryId }) => {
    const dispatch = useAppDispatch();
=======
const ModalDeleteCategory: FC<modalDeleteCategoryProps> = ({
  name,
  id,
}) => {
  const dispatch = useAppDispatch();
  
>>>>>>> Stashed changes
>>>>>>> Stashed changes

  const handleHideModal = () =>
    dispatch(hideModal({ name: modalsEnums.modalDeleteCategory }));

<<<<<<< Updated upstream
  const handleRemoveCategory = () => {
    dispatch(removeCategory(categoryId));
=======
<<<<<<< Updated upstream
    const handleRemoveCategory = () =>{ dispatch(removeCategory(categoryId));
=======
  const handleDeleteTodo = () => {
    dispatch(removeCategory(id));
    console.log('name', name)
>>>>>>> Stashed changes
>>>>>>> Stashed changes
    handleHideModal();
  };

  return (
    <Modal
      open={name === modalsEnums.modalDeleteCategory}
      aria-labelledby="child-modal-title"
      aria-describedby="child-modal-description"
    >
      <Box sx={{ ...style, width: 400 }}>
        <h2 id="child-modal-title">{name}</h2>
        <p id="child-modal-description">
          Вы действительно хотите удалить подзадачу?
        </p>
        <Box className={styles.buttons}>
<<<<<<< Updated upstream
          <Button
            variant="contained"
            color="error"
            onClick={handleRemoveCategory}
          >
=======
<<<<<<< Updated upstream
          <Button variant="contained" color="error" onClick={handleRemoveCategory}>
=======
          <Button
            variant="contained"
            color="error"
            onClick={handleDeleteTodo}
          >
>>>>>>> Stashed changes
>>>>>>> Stashed changes
            Удалить
          </Button>
          <Button variant="outlined" onClick={handleHideModal}>
            Закрыть
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default ModalDeleteCategory;
