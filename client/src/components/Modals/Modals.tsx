import React, {FC} from "react";
import { useAppSelector } from '../../store/hook';
import ModalDeleteCategory from './ModalDeleteCategory/ModalDeleteCategory'
import ModalEditCategory from "./ModalEditCategory/ModalEditCategory";

const modalCollections = {
    modalDeleteCategory: ModalDeleteCategory,
    modalEditCategory: ModalEditCategory
}

const Modals:FC = () => {
  const { modals } = useAppSelector((state)=>state.modals)

  if(!modals.length){
    return null
  }
  return <>
  {modals.map((item)=>{
       const CurrentModal = modalCollections[item.name as keyof typeof modalCollections];
       return <CurrentModal {...item} key={item.name}/>
  })}
  </>
}

export default Modals;