import React, { FC } from "react";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { useAppDispatch, useAppSelector } from "../../../store/hook";
import { hideModal, modalsEnums } from "../../../store/modals/slice";
import Todos from "../../Todos/Todos";
import Grid from "@mui/material/Grid";
import { Controller, useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import { Checkbox, TextField, FormControlLabel } from "@mui/material";
import { editCategory } from "../../../store/category/slice";
import styles from "./modalEditCategory.module.scss";

type ModalEditCategoryProps = {
  name: string;
  categoryId?: number;
};

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 2,
};

type CategoryFormType = {
  category?: string;
  completed?: boolean;
  description?: string;
};

let schema = yup.object().shape({
  category: yup.string().required("Должна быть подзадача"),
});

const ModalEditCategory: FC<ModalEditCategoryProps> = ({
  name,
  categoryId,
}) => {
  const dispatch = useAppDispatch();
  const { category } = useAppSelector((state) => state.category);
  const categoryFind = category.find((item) => item.id === categoryId);

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      category: categoryFind?.category,
      completed: categoryFind?.completed,
      description: categoryFind?.description,
    },
    resolver: yupResolver(schema),
  });

  const handleHideModal = () =>
    dispatch(hideModal({ name: modalsEnums.modalEditCategory }));

  const onSubmit = (data: CategoryFormType) => {
    dispatch(editCategory({ categoryId, categoryForm: data }));
    handleHideModal();
  };

  return (
    <Modal
      open={name === modalsEnums.modalEditCategory}
      aria-labelledby="child-modal-title"
      aria-describedby="child-modal-description"
    >
      <Box sx={{ ...style, width: 900 }}>
        <h2 id="child-modal-title">
          {" "}
          Вы редактируете подзадачу:{categoryFind?.category}
        </h2>
        <Grid container spacing={2}>
          <Grid item xs={6} md={4}>
            <Todos
              name={name}
              categoryId={categoryId}
              className={styles.todos}
            />
          </Grid>
          <Grid item xs={6} md={8}>
            <form onSubmit={handleSubmit(onSubmit)} className={styles.form}>
              <Controller
                control={control}
                name="category"
                render={({ field }) => (
                  <TextField
                    placeholder="подзадача"
                    error={!!errors.category}
                    helperText={errors?.category?.message}
                    size="small"
                    fullWidth
                    {...field}
                  />
                )}
              />
              <Controller
                control={control}
                name="completed"
                render={({ field }) => (
                  <FormControlLabel
                    label="Подзадача выполнена"
                    control={<Checkbox {...field} checked={field.value} />}
                  />
                )}
              />
              <Controller
                control={control}
                name="description"
                render={({ field }) => (
                  <TextField
                    placeholder="Описание подзадачи"
                    rows={8}
                    maxRows={10}
                    multiline
                    size="small"
                    fullWidth
                    {...field}
                  />
                )}
              />
              <Button variant="outlined" type="submit">
                Обновить
              </Button>
            </form>
          </Grid>
        </Grid>
        <Box className={styles.buttons}>
          <Button variant="outlined" onClick={handleHideModal}>
            Закрыть
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default ModalEditCategory;
