import React, { BaseSyntheticEvent, useState, FC } from "react";
import { useAppDispatch, useAppSelector } from "../../store/hook";
import AddTodo from "./AddTodo/AddTodo";
import EditIcon from "@mui/icons-material/Edit";
import DeleteIcon from "@mui/icons-material/Delete";
// import ListItem from "@mui/material/ListItem";
// import ListItemButton from "@mui/material/ListItemButton";
import UndoIcon from "@mui/icons-material/Undo";
import ListItemText from "@mui/material/ListItemText";
import { removeTodo } from "../../store/todo/slice";
import EditTodo from "./EditTodo/EditTodo";
import { NavLink, useMatch, useNavigate } from "react-router-dom";
import cn from 'classnames';
import styles from "./todo.module.scss";
import { hideModal, modalsEnums } from "../../store/modals/slice";
import { Box } from "@mui/material";
import { transferCategory } from "../../store/category/slice";
import { Lists, ListItem } from "../Lists/Lists";

type TodosProps = {
  name?: string;
  categoryId?: number;
  className?:string;
};

const Todos: FC<TodosProps> = ({ name, categoryId,className }) => {
  const { todos } = useAppSelector((state) => state.todos);
  const [todoId, setTodoId] = useState<number>(0);
  const [visible, setVisible] = useState(false);
  const findTodo = todos.find((item) => item.id === todoId);
  const dispatch = useAppDispatch();
  const history = useNavigate();
  const match = useMatch('/todos/:id');
  const matchTodoId = Number(match?.params?.id);

  const handleRemoveTodo = (todoId: number) => {
    dispatch(removeTodo(todoId));
  };
  const handleEditTodo = (todoId: number) => {
    setTodoId(todoId);
    setVisible(!visible);
  };

  const handleTransferCategory = (todoId:number) => {
    dispatch(transferCategory({categoryId,todoId }));
    dispatch(hideModal({name:modalsEnums.modalEditCategory}));
    history(`/todos/${todoId}`)
  }

  const isModalEditCategory = name === modalsEnums.modalEditCategory;

  return (
    <>
      {!isModalEditCategory && <AddTodo />}
  <div className={styles.wrapper}>
      <Lists className={className}>
        {todos.map((item) => (
          <>
            <ListItem key={item.id} className={cn(styles.todosList,{[styles.active]:matchTodoId===item.id})}>
              <NavLink to={!isModalEditCategory ? `/todos/${item.id}`: '#'} className={styles.link}>
                <ListItemText primary={item.title} />
              </NavLink>

              {!isModalEditCategory ? (
                <Box>
                  <EditIcon onClick={() => handleEditTodo(item.id)} />
                  {(visible && item.id === todoId) || (
                    <DeleteIcon onClick={() => handleRemoveTodo(item.id)} />
                  )}
                </Box>
              ) : (
                <UndoIcon onClick={()=>handleTransferCategory(item.id)}/>
              )}
            </ListItem>

            {visible && item.id === todoId && (
              <EditTodo todoId={todoId} title={findTodo?.title} />
            )}
          </>
        ))}
      </Lists>
      </div>
    </>
  );
};

export default Todos;
