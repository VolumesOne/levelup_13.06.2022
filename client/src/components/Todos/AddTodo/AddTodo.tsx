import React from "react";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import { useAppDispatch } from "../../../store/hook";
import { addTodo } from "../../../store/todo/slice";
import { useForm, Controller } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import styles from "./addTodo.module.scss";
type AddTodoForm = {
  title: string;
};

let schema = yup.object().shape({
  title: yup.string().required("Поле не должно быть пустым"),
});

const AddTodo = () => {
  const {
    control,
    handleSubmit,
    setValue,
    formState: { errors },
  } = useForm({
    defaultValues: {
      title: "",
    },
    resolver: yupResolver(schema),
  });

  const dispatch = useAppDispatch();

  const onSubmit = (data: AddTodoForm) => {
    dispatch(addTodo({ ...data, id: Date.now() }));
    setValue("title", "");
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Controller
        name="title"
        control={control}
        render={({ field }) => (
          <TextField
            {...field}
            helperText={errors?.title?.message}
            error={!!errors?.title}
            size="small"
            id="outlined-basic"
            label="Добавить запись"
            variant="outlined"
          />
        )}
      />

      <Button variant="contained" className={styles.button} type="submit">
        Добавить
      </Button>
    </form>
  );
};

export default AddTodo;
