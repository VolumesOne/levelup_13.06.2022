import React, { BaseSyntheticEvent, FC, useState } from "react";
import { Button, TextField, Box } from "@mui/material";
import styles from "./editTodo.module.scss";
import { useAppDispatch } from "../../../store/hook";
import { updateTodo } from "../../../store/todo/slice";
import { useForm, Controller } from "react-hook-form";

type EditTodoProps = {
  title?: string;
  todoId: number;
};

const EditTodo: FC<EditTodoProps> = ({ title, todoId }) => {
  const [value, setValue] = useState(title);
  const dispatch = useAppDispatch();

  const handleOnChangeInput = (e: BaseSyntheticEvent) => {
    setValue(e.target.value);
  };

  const handleEditTodo = () => {
    const obj = {
      title: value,
      todoId,
    };

    dispatch(updateTodo(obj));
  };

  return (
    <Box className={styles.box}>
      <TextField
        value={value}
        onChange={handleOnChangeInput}
        size="small"
        id="outlined-basic"
        label="Редактировать запись"
        variant="outlined"
      />

      <Button variant="contained" onClick={handleEditTodo}>
        Редактировать
      </Button>
    </Box>
  );
};

export default EditTodo;
