import React, { FC } from "react";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import styles from './progressbar.module.scss';
import { useAppSelector } from "../../store/hook";
import { useMatch } from "react-router-dom";

const ProgressBar: FC = () => {
  const { category } = useAppSelector((state) => state.category);
  const { todos } = useAppSelector((state) => state.todos);

  const match = useMatch('todos/:id');
  const matchTodoId = Number(match?.params?.id);

  const findTodo = todos.find((item) => item.id === matchTodoId);
  const filterCategoryTodo = category.filter((item) => item.todoId === matchTodoId);

  const filterCategoryCompleted = filterCategoryTodo.filter((item) => item.completed);

  const completedCategory = (100 * filterCategoryCompleted.length / filterCategoryTodo.length);

  return (
    <Box sx={{ width: "100%" }} className={styles.wrapper}>
       {findTodo?.title ? `Прогресс выполнения задачи ${findTodo?.title}` : ''}
      <LinearProgress variant="determinate" value={completedCategory || 0} className={styles.progress} />
    </Box>
  );
}


export default ProgressBar;