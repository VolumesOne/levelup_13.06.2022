import React from "react";
import "./App.css";

import { ThemeProvider, createTheme } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';

import ButtonCreateTodo from "./components/Buttons/ButtonCreateTodo/ButtonCreateTodo";
import Header from "./components/Header/Header";

import Modals from "./components/Modals/Modals";
import Todos from "./components/Todos/Todos";

import ThemeToggle from './components/Buttons/ThemeToggle';

import { ColorModeContext } from './theme-context.js';

function App() {

  const [mode, setMode] = React.useState<'light' | 'dark'>('light');
  const colorMode = React.useMemo(
    () => ({
      toggleColorMode: () => {
        setMode((prevMode) => (prevMode === 'light' ? 'dark' : 'light'));
      },
    }),
    [],
  );

  const theme = React.useMemo(
    () =>
      createTheme({
        palette: {
          mode,
        },
      }),
    [mode],
  );

  return (
    <ColorModeContext.Provider value={colorMode}>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <ThemeToggle />
      <Header />
      <Todos/>
      <ButtonCreateTodo/>
      <Modals/>
    </ThemeProvider>
    </ColorModeContext.Provider>
  );
}

export default App;
