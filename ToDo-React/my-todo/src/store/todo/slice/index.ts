import {createSlice} from '@reduxjs/toolkit';


type Todos = {
    name:string;
    description:string;
    id:number;
}

interface ITodosState {
    todos: Todos[]
}

const initialState: ITodosState = {
    todos: [{name:'Название', description:'Описание', id:2}]
}

export const todosSlice = createSlice({
    name:'todos',
    initialState,
    reducers: {
        addTodo:(state, {payload})=>({
            ...state, todos: [...state.todos, payload]
        }),
        deleteTodo:(state, {payload}) =>({
            ...state, todos: state.todos.filter((item) => item.id !== payload)
        }),
        updateTodo:(state, {payload}) =>({
            ...state, todos:state.todos.map((item)=> item.id === payload.id ? {...item, name: payload.name, description: payload.description } : item)
        })

    
    },
})

export const {addTodo, deleteTodo,updateTodo} = todosSlice.actions

export default todosSlice.reducer