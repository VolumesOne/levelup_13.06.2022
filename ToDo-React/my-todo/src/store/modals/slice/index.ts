import {createSlice} from '@reduxjs/toolkit';

type modalType ={
    name:modalsEnums;
    description:modalsEnums;
    id:number;
    text: string;
}

export enum modalsEnums {
    modalAddTodo = 'modalAddTodo',
    modalDeleteTodo = 'modalDeleteTodo',
    modalEditTodo = 'modalEditTodo'
   
}

type initialStateType = {
    modals:modalType[];
}

const initialState:initialStateType = {
    modals : [],
}

export const modalsSlice = createSlice({
    name:'modals',
    initialState,
    reducers: {
        showModal:(state, {payload})=>({
            ...state, modals: [...state.modals, payload]
        }),
        hideModal:(state, {payload}) =>({
            ...state, modals:state.modals.filter((item)=>item.name !== payload.name)
        })
    },
})

export const {showModal, hideModal } = modalsSlice.actions

export default modalsSlice.reducer