import { Box } from "@mui/material";
import { SearchInput } from "evergreen-ui";
import React, { BaseSyntheticEvent, useEffect, useState } from "react";
import { useAppDispatch, useAppSelector } from "../../store/hook";
import ButtonSearch from "../Buttons/ButtonSearch/ButtonSerach";
import Todos from "../Todos/Todos";
import styles from "./searchbar.module.scss";



const Searchbar = () => {
  const filter  = useAppSelector((state) => state.todos);

  const dispatch = useAppDispatch();

  const [searchTodoName, setSearchTodoName] = useState("");
  const handleChange = (event: BaseSyntheticEvent) => {
    event.preventDefault();
    setSearchTodoName(event.target.value);
  };
  const [filteredTodos, setFilteredTodos] = useState([]);

  useEffect(()=>{
     filter.todos.filter((el)=>{
        let item = el.name.toLowerCase();
        return item.includes(searchTodoName.toLowerCase())
      });
      setFilteredTodos(filteredTodos)
  },[filter, searchTodoName]);


  return (
    <Box className={styles.search__wrapper}>
      <SearchInput
        placeholder="Поиск..."
        className={styles.searchbar}
        type="text"
        value={searchTodoName}
        onChange={handleChange}
      /> 

      <ButtonSearch />
    </Box>
  );
};

export default Searchbar;
