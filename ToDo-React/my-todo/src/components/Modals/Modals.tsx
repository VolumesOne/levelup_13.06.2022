import React, {FC} from "react";
import { useAppSelector } from '../../store/hook';
import ModalDeleteTodo from '../Modals/ModalDeleteTodo/ModalDeleteTodo';
import ModalAddTodo from "./ModalAddTodo/ModalAddTodo";
import ModalEditTodo from "./ModalEditTodo/ModalEditTodo";


const modalCollections = {
    modalAddTodo: ModalAddTodo,
    modalDeleteTodo: ModalDeleteTodo,
    modalEditTodo: ModalEditTodo
}

const Modals:FC = () => {
  const { modals } = useAppSelector((state)=>state.modals)

  if(!modals.length){
    return null
  }
  return <>
  {modals.map((item)=>{
        const CurrentModal = modalCollections[item.name as keyof typeof modalCollections];
        return <CurrentModal {...item} key={item.name} />
  })}
  </>
}

export default Modals;