import React, { FC, useState } from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";
import { hideModal, modalsEnums } from "../../../store/modals/slice";
import { deleteTodo, todosSlice } from "../../../store/todo/slice";
import { useAppDispatch, useAppSelector } from "../../../store/hook";
import styles from "./modalDeleteTodo.module.scss";
import { isTemplateExpression } from "typescript";
import Todos from "../../Todos/Todos";

type modalDeleteTodoProps = {
  name: string;
  id: number;
  description: string;
  text: string;
};

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const ModalDeleteTodo: FC<modalDeleteTodoProps> = ({ name, id, text }) => {
  const dispatch = useAppDispatch();

  const handleHideModal = () =>
    dispatch(hideModal({ name: modalsEnums.modalDeleteTodo }));

  const handleDeleteTodo = () => {
    dispatch(deleteTodo(id));
    handleHideModal();
  };

  return (
    <Modal
      open={name === modalsEnums.modalDeleteTodo}
      aria-labelledby="child-modal-title"
      aria-describedby="child-modal-description"
    >
      <Box sx={{ ...style, width: 400 }}>
        <h2 id="child-modal-title">{text}</h2>
        <p id="child-modal-description">
          Вы действительно хотите удалить эту подзадачу?
        </p>
        <Box className={styles.btn__wrapper}>
          <Button variant="contained" color="error" onClick={handleDeleteTodo}>
            Удалить
          </Button>
          <Button variant="outlined" onClick={handleHideModal}>
            Закрыть
          </Button>
        </Box>
      </Box>
    </Modal>
  );
};

export default ModalDeleteTodo;
