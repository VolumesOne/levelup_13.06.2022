import React, { BaseSyntheticEvent, FC, useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";

import { addTodo } from "../../../store/todo/slice";
import { useAppDispatch, useAppSelector } from "../../../store/hook";
import { hideModal, modalsEnums, showModal } from "../../../store/modals/slice";

import { TextField } from "@mui/material";
import styles from "./modalAddTodo.module.scss";

interface modalAddTodoProps {
  name: string,
  description?: string,
  id?: number,
};

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  p: 4,
};


const ModalAddTodo: FC<modalAddTodoProps> = ({ name }) => {
  const DEFAULT_TODO = {
    name: "",
    description: "",
    id: Date.now(),
  };

  const [todo, setTodo] = useState(DEFAULT_TODO);
  const [nameError, setNameError] = useState(false);
  const dispatch = useAppDispatch();

  const handleOnChangeTitle = (event: BaseSyntheticEvent) => {
    setNameError(false);
    const { name, value } = event.target;
    setTodo({ ...todo, [name]: value });
    value.length > 30 && setNameError(true);
  };

  const handleOnChangeValue = (event: BaseSyntheticEvent) => {
    const { name, value } = event.target;
    setTodo({ ...todo, [name]: value });
  };

  const handleHideModal = () =>
    dispatch(hideModal({ name: modalsEnums.modalAddTodo }));

  const handleAddTodo = () => {
    dispatch(addTodo(todo));
    handleHideModal();
  };

  return (
    <form>
      <Modal
        open={name === modalsEnums.modalAddTodo}
        aria-labelledby="modal-modal-name"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h3">
            Введите заголовок заметки
            <TextField
              name="name"
              id="name"
              value={todo.name}
              error={nameError}
              autoComplete="off"
              placeholder="Введите заголовок...*"
              onChange={handleOnChangeTitle}
              className={styles.input}
            />
          </Typography>

          <Typography id="modal-modal-title" variant="h6" component="h3">
            Введите текст заметки
            <TextField
              name="description"
              id="description"
              value={todo.description}
              autoComplete="off"
              placeholder="Введите текст..."
              onChange={handleOnChangeValue}
              className={styles.input}
            />
          </Typography>
          <Box className={styles.wrapper__buttons}>
            <Button
              variant="outlined"
              onClick={handleAddTodo}
              type="submit"
            >
              Создать
            </Button>
            <Button variant="contained" onClick={handleHideModal}>
              Закрыть
            </Button>
          </Box>
        </Box>
      </Modal>
    </form>
  );
};

export default ModalAddTodo;
