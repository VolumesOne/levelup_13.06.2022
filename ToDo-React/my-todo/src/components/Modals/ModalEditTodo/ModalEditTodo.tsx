import React, { BaseSyntheticEvent, FC, useState } from "react";
import { Button, TextField, Box, Modal, Typography } from "@mui/material";
import { useAppDispatch } from "../../../store/hook";
// import { updateTodo } from "../../../store/todo/slice";
import { useForm, Controller } from "react-hook-form";
import { hideModal, modalsEnums } from "../../../store/modals/slice";
import { updateTodo } from "../../../store/todo/slice";
import { TextInput } from "evergreen-ui";
import styles from './modalEditTodo.module.scss';




    type modalEditTodoProps = {
        name: string;
        description?: string;
        id?: number;
        text?: string;
      };
      
      
    const style = {
        position: "absolute" as "absolute",
        top: "50%",
        left: "50%",
        transform: "translate(-50%, -50%)",
        width: 400,
        bgcolor: "background.paper",
        p: 4,
      };
      

const ModalEditTodo:FC<modalEditTodoProps> = ({name, description, id, text}) =>{
  const [todo, setTodo] = useState({
    name: text,
    description: description,
    id: id,
  });

  const dispatch = useAppDispatch();

  const handleHideModal = () =>
    dispatch(hideModal({ name: modalsEnums.modalEditTodo }));
    
  const handleOnChangeValue = (event: BaseSyntheticEvent) => {
    const { name, value } = event.target;
    setTodo({ ...todo, [name]: value });
  };

  const handleEditTodo = () => {
    dispatch(updateTodo(todo));
    handleHideModal();
  }


  return (
    <div>
      <Modal
        open={name === modalsEnums.modalEditTodo}
        aria-labelledby="child-modal-title"
        aria-describedby="child-modal-description"
      >
      <Box sx={{ ...style, width: 400 }}>
        <h2 id="child-modal-title">{text}</h2>
        <p id="child-modal-description">
          Вы действительно хотите редактировать эту подзадачу?
        </p>
        <div className={styles.textfield__wrapper}>
          <TextField
            name="name"
            id="name"
            value={todo.name}
            autoComplete="off"
            placeholder="Введите заголовок...*"
            onChange={handleOnChangeValue}
            className={styles.input}
          />
          <TextField
            name="description"
            id="description"
            value={todo.description}
            autoComplete="off"
            placeholder="Введите текст..."
            onChange={handleOnChangeValue}
            className={styles.input}
          />
        </div>
        <Box className={styles.btn__wrapper}>
          <Button variant="contained" color="secondary" onClick={handleEditTodo}>
            Редактировать
          </Button>
          <Button variant="outlined" onClick={handleHideModal}>
            Закрыть
          </Button>
        </Box>
      </Box>
    </Modal>
    </div>
  )
}

export default ModalEditTodo;