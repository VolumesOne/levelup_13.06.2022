import React from "react";
import Box from "@mui/material/Box";
import Searchbar from "../Searchbar/Searchbar";
import styles from "./header.module.scss";

const Header = () => {
  return (
    
    <Box className={styles.header__wrapper}>

    <Searchbar />
    </Box>

  );
};

export default Header;
