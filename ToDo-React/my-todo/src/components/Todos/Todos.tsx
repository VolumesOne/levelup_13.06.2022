import React, {BaseSyntheticEvent, FC, useState} from "react";
import Box from '@mui/material/Box';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import { idText } from "typescript";
import DeleteSweepIcon from '@mui/icons-material/DeleteSweep';
import { useAppDispatch, useAppSelector } from "../../store/hook";
import styles from './todos.module.scss';
import { modalsEnums, showModal } from "../../store/modals/slice";

type Todos = {
  name:string;
  description:string;
  id:number;
}

const Todos = () => {
  const {todos} = useAppSelector((state) => state.todos);
 

  const dispatch = useAppDispatch();


  const handleEditTodo = (description: string, text: string, id: number) => {
    dispatch(showModal({name: modalsEnums.modalEditTodo, description, text, id}))
  }

  const handleDeleteTodoItem = (event:BaseSyntheticEvent, id:number) => {
    event.preventDefault();
    dispatch(showModal({ name: modalsEnums.modalDeleteTodo, id }));

}

 


    return <>

 <Box className={styles.wrapper} >
   {todos.map((item) => <ListItem component="div" key={item.id} disablePadding className={styles.todo__wrapper}>
      <ListItemButton  onClick={() => handleEditTodo(item.description, item.name, item.id )} className={styles.style__box} >
    
        <Box className={styles.todo__box}>
    
        <ListItemText primary={item.name} className={styles.title__box}/>
        <Box className={styles.descr__delete}>
        <ListItemText primary={item.description} className={styles.descr__box}/>
       
        </Box>
       
        </Box>
    
      </ListItemButton>
        <DeleteSweepIcon className={styles.btn__del} onClick={(event)=>handleDeleteTodoItem(event, item.id)}/>
    </ListItem>)}
    </Box> 

    </>
}

export default Todos;