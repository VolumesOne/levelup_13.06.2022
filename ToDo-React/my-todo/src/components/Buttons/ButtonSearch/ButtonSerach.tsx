import React from "react";
import { Button, SearchIcon } from "evergreen-ui";
import styles from './buttonsearch.module.scss';


const ButtonSearch = () => {
  return (
    <Button marginY={8} marginRight={12} iconBefore={SearchIcon}>
      Поиск
    </Button>
  );
};

export default ButtonSearch;
