import React from "react";
import Box from '@mui/material/Box';
import Fab from '@mui/material/Fab';
import AddIcon from '@mui/icons-material/Add';
import styles from './buttoncreatetodo.module.scss';
import { useAppDispatch } from "../../../store/hook";
import { modalsEnums, showModal } from "../../../store/modals/slice";


const ButtonCreateTodo = () => {
  const dispatch = useAppDispatch();
  const handleShowModal = () => 
    dispatch(showModal({ name: modalsEnums.modalAddTodo }));
  return (
    <Box sx={{ '& > :not(style)': { m: 1 } }} >
    <Fab aria-label="add" color='warning' className={styles.icon__addtodo} onClick={handleShowModal}>
      <AddIcon  />
    </Fab>
  </Box>
  );
}

export default ButtonCreateTodo;