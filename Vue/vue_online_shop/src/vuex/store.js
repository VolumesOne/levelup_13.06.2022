import { createStore } from "vuex";

export default createStore({
  state: {
    products: [
      {
        image: "1.jpg",
        name: "Converse Kids 70",
        price: 49.99,
        article: "T1",
        discount: "full price",
        category: "sneaker",
      },
      {
        image: "2.jpg",
        name: "Converse Chuck 70",
        price: 49.99,
        article: "T2",
        discount: "full price",
        category: "sneaker",
      },
      {
        image: "3.jpg",
        name: "Converse Chuck 70 Renew High Top ",
        price: 84.99,
        article: "T3",
        discount: "bestseller",
        category: "sneaker",
      },
      {
        image: "4.jpg",
        name: "Converse Pro Chuck 80",
        price: 99.99,
        article: "T4",
        discount: "bestseller",
        category: "sneaker",
      },
      {
        image: "5.jpg",
        name: "Converse Winter Chuck 70",
        price: 99.99,
        article: "T5",
        discount: "bestseller",
        category: "sneaker",
      },
      {
        image: "6.jpg",
        name: "Converse Winter Chuck 70 Full Black",
        price: 129.99,
        article: "T6",
        discount: "full price",
        category: "sneaker",
      },
      {
        image: "7.jpg",
        name: "Converse Winter Chuck 70 Black/White",
        price: 99.99,
        article: "T7",
        discount: "full price",
        category: "no-sneaker",
      },
    ],
    basket: [],
    filteredProducts: [],
  },
  getters: {
    productsItems: (state) => state.filteredProducts,
    basketItems: (state) => state.basket,
    totalPrice: (state) => {
      let total = state.basket.reduce((acc, item) => {
        return acc + item.price;
      }, 0);
      return total.toFixed(2);
    },
  },

  mutations: {
    UPDATE_BASKET(state, payload) {
      state.basket.push(payload);
    },

    REMOVE_BASKET(state, payload) {
      const index = state.basket.findIndex(
        (e) => e.article === payload.article
      );
      state.basket.splice(index, 1);
    },

    FILTER_ITEMS(state, payload) {
      let filtered = state.products;
      if (!payload) {
        return filtered;
      }
      for (let index = 0; index < payload.length; index++) {
        const filterItem = payload[index];
        const filterKey = Object.keys(filterItem);
        filtered = filtered.filter(
          (i) => i[filterKey] === filterItem[filterKey]
        );
      }
      state.filteredProducts = filtered;
    },

    REMOVE_ALL_BASKET(state) {
      state.basket = [];
    },
    SEARCH_ITEM(state, payload) {
      state.filteredProducts = state.products.filter((product) => {
        return product.name.toLowerCase().includes(payload.toLowerCase());
      });
    },
  },
  actions: {
    filter({ commit }, filter) {
      commit("FILTER_ITEMS", filter);
    },
    updateBasket({ commit }, item) {
      commit("UPDATE_BASKET", item);
    },
    removeBasketItem({ commit }, item) {
      commit("REMOVE_BASKET", item);
    },
    removeAllBasketItem({ commit }, item) {
      commit("REMOVE_ALL_BASKET", item);
    },
    search({ commit }, searchItem) {
      commit("SEARCH_ITEM", searchItem);
    },
  },
});
