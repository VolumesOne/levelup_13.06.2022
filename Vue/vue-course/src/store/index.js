import { createStore } from 'vuex'

export default createStore({
  state: {
    posts:[]
  },
  getters: {
    getPosts:(state)=>state.posts
  },
  mutations: {
    setPosts(state, value){
      state.posts = value
    }
  },
  actions: {
    updatePosts({commit}){
      fetch('https://jsonplaceholder.typicode.com/posts')
     .then(response => response.json())
     .then(json => commit('setPosts', json))
    }
  }
})
